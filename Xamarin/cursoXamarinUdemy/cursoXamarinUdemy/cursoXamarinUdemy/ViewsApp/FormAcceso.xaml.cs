﻿using cursoXamarinUdemy.ViewsApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cursoXamarinUdemy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormAcceso : ContentPage
	{
		public FormAcceso ()
		{
			InitializeComponent ();
		}

        private async void butAceptar_Clicked(object sender, EventArgs e)
        {
			await Navigation.PushAsync(new pagPrincipal());
        }
    }
}