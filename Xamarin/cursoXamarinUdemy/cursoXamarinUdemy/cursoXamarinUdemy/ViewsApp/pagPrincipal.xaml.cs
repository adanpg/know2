﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cursoXamarinUdemy.ViewsApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class pagPrincipal : ContentPage
	{
		public pagPrincipal ()
		{
			InitializeComponent ();
		}

        private async void butRegresar_Clicked(object sender, EventArgs e)
        {
			await Navigation.PopAsync();
		}

        private void butLeer_Clicked(object sender, EventArgs e)
        {
			DisplayAlert("Valores","editor " + entEditor.Text + " Fecha : " + pikFecha.Date, "Aceptar");
        }

        private void butCat_Clicked(object sender, EventArgs e)
        {

        }
    }
}