﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin2023.MyClases;

namespace Xamarin2023.MyViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class pagAcceso : ContentPage
    {
        public pagAcceso()
        {
            InitializeComponent();
        }

        private async void butAceptar_Clicked(object sender, EventArgs e)
        {
            var claMYU = new claUser();

            claMYU.strName = entName.Text;
            claMYU.strPass = entPass.Text;

            if (claMYU.bolAccept(claMYU.strName,claMYU.strPass) )
            {
                //un detalle: este push agrega un boton de regrersar, pero ese boton no es correcto cuando la pagina
                //es de unicio, pues no tiene caso que te devuelva al login
                //await Navigation.PushAsync(new pagPrincipal());

                //alternativa que no muestra boton
                Application.Current.MainPage = new pagPrincipal();
            }
            else {
                await DisplayAlert("Alert", "Error de acceso", "OK");
            }

            
        }
    }
}