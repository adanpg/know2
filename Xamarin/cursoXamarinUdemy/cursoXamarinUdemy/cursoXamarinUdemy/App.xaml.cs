﻿using cursoXamarinUdemy.Services;
using cursoXamarinUdemy.Views;
using cursoXamarinUdemy.ViewsApp;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cursoXamarinUdemy
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();

            //solo se cambia el nombre de la pagina tipo xaml, la personalizada
            MainPage =new NavigationPage( new FormAcceso());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
