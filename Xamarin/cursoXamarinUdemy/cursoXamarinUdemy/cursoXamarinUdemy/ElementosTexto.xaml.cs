﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cursoXamarinUdemy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ElementosTexto : ContentPage
	{
		public ElementosTexto ()
		{
			InitializeComponent ();
		}

        private void butMas_Clicked(object sender, EventArgs e)
        {
            //Application.Current.MainPage.DisplayAlert("ddd","mensaje","salir");
            DisplayAlert("ddd", "te voy a mostrar la linea completa de texto", "salir");
			labTrunco.LineBreakMode=LineBreakMode.WordWrap;
			
        }
    }
}