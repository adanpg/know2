﻿using MDok.myViews;
using MDok.Services;
using MDok.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MDok
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new myFlyoutPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
