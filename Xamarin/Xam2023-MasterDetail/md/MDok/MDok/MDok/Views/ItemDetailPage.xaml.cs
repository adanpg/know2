﻿using MDok.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace MDok.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}