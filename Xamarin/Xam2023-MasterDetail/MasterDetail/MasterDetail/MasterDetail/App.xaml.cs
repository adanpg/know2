﻿using MasterDetail.myViews;
using MasterDetail.Services;
using MasterDetail.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MasterDetail
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new md();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
