﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterDetail.myClases
{
    internal class claFlyoutItemPage
    {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetPage { get; set; }
    }
}
