﻿using MasterDetail.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace MasterDetail.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}