﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xamarin2023.MyClases
{
    public class claCategoria
    {
        public int IdCategoria { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
