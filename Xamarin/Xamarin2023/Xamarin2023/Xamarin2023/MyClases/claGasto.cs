﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xamarin2023.MyClases
{
    public class claGasto
    {
        public int idGasto { get; set; }
        public string Descrpcion { get; set; }
        public DateTime Fecha { get; set; }
        public double Monto { get; set; }
        public string Notas { get; set; }
    }
}
