﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin2023.MyViews;
using Xamarin2023.Services;
using Xamarin2023.Views;

namespace Xamarin2023
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            //para volver a las paginas navegables
            DependencyService.Register<MockDataStore>();
            MainPage = new NavigationPage(new pagAcceso());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
