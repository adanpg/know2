﻿using System.ComponentModel;
using Xamarin.Forms;
using Xamarin2023.ViewModels;

namespace Xamarin2023.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}