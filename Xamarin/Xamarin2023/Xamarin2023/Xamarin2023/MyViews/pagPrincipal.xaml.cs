﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin2023.MyClases;

namespace Xamarin2023.MyViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class pagPrincipal : ContentPage
    {
        //esta instancia es la que se va a referenciar a la hora de utilizar los datos de la lista 
        public List<claCategoria> ListaCategoria { get; set; }
        public pagPrincipal()
        {

            //una opcion mas utilizada seria implementar este codigo en el constructor de la clase claCategoria
            InitializeComponent();
            ListaCategoria = new List<claCategoria>();
            ListaCategoria.Add(new claCategoria {IdCategoria = 1, Descripcion = "alimentos con proteina", Nombre = "atun" });
            ListaCategoria.Add(new claCategoria {IdCategoria = 2, Descripcion = "alimentos con proteina", Nombre = "avena" });
            ListaCategoria.Add(new claCategoria {IdCategoria = 3, Descripcion = "alimentos con carbohidratos", Nombre = "papa" });
            
            
            //cuidado con esa instruccion. NO se realmente 
            BindingContext = this;
        }
    }
}