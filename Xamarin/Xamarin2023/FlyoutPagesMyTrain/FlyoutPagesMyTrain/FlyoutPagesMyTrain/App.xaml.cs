﻿using FlyoutPagesMyTrain.myViews;
using FlyoutPagesMyTrain.Services;
using FlyoutPagesMyTrain.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FlyoutPagesMyTrain
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new myFlyoutPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
