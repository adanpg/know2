﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FlyoutPagesMyTrain.myViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class myFlyoutPageFlyout : ContentPage
    {
        public ListView ListView;

        public myFlyoutPageFlyout()
        {
            InitializeComponent();

            BindingContext = new myFlyoutPageFlyoutViewModel();
            ListView = MenuItemsListView;
        }

        private class myFlyoutPageFlyoutViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<myFlyoutPageFlyoutMenuItem> MenuItems { get; set; }

            public myFlyoutPageFlyoutViewModel()
            {
                MenuItems = new ObservableCollection<myFlyoutPageFlyoutMenuItem>(new[]
                {
                    new myFlyoutPageFlyoutMenuItem { Id = 0, Title = "Microsoft", TargetType=typeof(pagMicrosoft) },
                    new myFlyoutPageFlyoutMenuItem { Id = 1, Title = "Diseño (free)", TargetType=typeof(pagDesign)  },
                    new myFlyoutPageFlyoutMenuItem { Id = 2, Title = "Programación (free)", TargetType=typeof(pagProgramation)  },
                    new myFlyoutPageFlyoutMenuItem { Id = 3, Title = "Sistemas Operativos", TargetType=typeof(pagSO)  },
                    new myFlyoutPageFlyoutMenuItem { Id = 4, Title = "Acerca de...", TargetType=typeof(pagAbout)  },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}