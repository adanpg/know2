﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyoutPagesMyTrain.myViews
{
    public class myFlyoutPageFlyoutMenuItem
    {
        public myFlyoutPageFlyoutMenuItem()
        {
            TargetType = typeof(myFlyoutPageFlyoutMenuItem);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}