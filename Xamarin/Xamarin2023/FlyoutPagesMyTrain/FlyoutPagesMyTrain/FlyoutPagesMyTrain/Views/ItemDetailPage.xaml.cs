﻿using FlyoutPagesMyTrain.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace FlyoutPagesMyTrain.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}